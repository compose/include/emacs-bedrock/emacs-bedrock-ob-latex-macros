;; Credits go to oantolin https://www.reddit.com/r/orgmode/comments/7u2n0h/tip_for_defining_latex_macros_for_use_in_both/

(use-package org-src
  :defer t
  :config
  (add-to-list 'org-src-lang-modes '("latex-macros" . latex)))

(use-package ob-core
  :defer t
  :custom
  (org-babel-default-header-args:latex-macros
   '((:results . "raw")
     (:exports . "results")
     (:tangle . "latex-macros.tex"))))

(use-package ob
  :defer t
  :after ob-core org-src
  :config
  (defun prefix-all-lines (pre body)
    (with-temp-buffer
      (insert body)
      (string-insert-rectangle (point-min) (point-max) pre)
      (buffer-string)))  
  ;; TODO refine to export only latex-macros
  (add-hook 'org-export-before-processing-hook #'org-babel-tangle)
  (defun org-babel-execute:latex-macros (body _params)
    (concat
     (prefix-all-lines "#+LATEX_HEADER: " body)
     "\n#+HTML_HEAD_EXTRA: <div style=\"display: none\"> \\(\n"
     (prefix-all-lines "#+HTML_HEAD_EXTRA: " body)
     "\n#+HTML_HEAD_EXTRA: \\)</div>\n")))

(provide 'elementaryx-ob-latex-macros)
